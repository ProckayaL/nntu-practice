package com.marukhinm.app_storage_async.list;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClickRV(View view, int position);
}
