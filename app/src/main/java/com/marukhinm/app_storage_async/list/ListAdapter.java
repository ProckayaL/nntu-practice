package com.marukhinm.app_storage_async.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.marukhinm.app_storage_async.R;
import com.marukhinm.app_storage_async.Task;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private RecyclerViewClickListener mListener;
    private Context mContext;
    private List<Task> mData;

    public ListAdapter(Context context, List<Task> data) {
        mContext = context;
        mListener = (RecyclerViewClickListener) context;
        mData = data;
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitle;
        private TextView mDescription;
        private ImageButton mMenu;
        private ImageView mFavourite;
        private RecyclerViewClickListener mListener;

        ListViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            mTitle = view.findViewById(R.id.titleText);
            mDescription = view.findViewById(R.id.descriptionText);
            mMenu = view.findViewById(R.id.menuItem);
            mFavourite = view.findViewById(R.id.favouriteImage);
            mListener = listener;
            view.setOnClickListener(this);
            mMenu.setOnClickListener(this);
            mFavourite.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onClickRV(v, getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.list_item, viewGroup, false);
        return new ListViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int position) {
        listViewHolder.mTitle.setText(mData.get(position).getTitle());
        listViewHolder.mDescription.setText(mData.get(position).getDescription());
        if (mData.get(position).isFavourite()) {
            listViewHolder.mFavourite.setImageResource(R.drawable.ic_star);
        } else {
            listViewHolder.mFavourite.setImageResource(R.drawable.ic_star_border);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
