package com.marukhinm.app_storage_async.storage;

import com.marukhinm.app_storage_async.Task;

import java.util.List;

public interface Helper {
    List<Task> load();

    void write(List<Task> list);

    void clear();
}
