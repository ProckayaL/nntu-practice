package com.marukhinm.app_storage_async.storage;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.marukhinm.app_storage_async.Task;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SQLHelper implements Helper {
    private static WeakReference<Context> sContext;
    private static AppDatabase sInstance;

    SQLHelper(Context context) {
        sContext = new WeakReference<>(context);
    }

    @Override
    public List<Task> load() {
        LoadDataTask load = new LoadDataTask();
        load.execute();
        try {
            return load.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public void write(List<Task> list) {
        WriteDataTask write = new WriteDataTask();
        write.execute(list);
    }

    @Override
    public void clear() {
        ClearDataTask clear = new ClearDataTask();
        clear.execute();
    }

    @Entity
    public static class sqlTask {
        @PrimaryKey
        @NonNull
        String title;
        String description;
        boolean isFavourite;
    }

    @Dao
    public interface sqlTaskDao {
        @Query("SELECT * FROM sqlTask")
        List<sqlTask> getAll();

        @Insert
        void insert(sqlTask sqltask);

        @Query("DELETE FROM sqlTask")
        void delete();
    }

    @Database(entities = {sqlTask.class}, version = 1, exportSchema = false)
    public static abstract class AppDatabase extends RoomDatabase {
        public abstract sqlTaskDao sqltaskDao();
    }

    private static AppDatabase getDatabase() {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(sContext.get(),
                    AppDatabase.class, "database")
                    .build();
        }
        return sInstance;
    }

    public static class LoadDataTask extends AsyncTask<Void, Integer, List<Task>> {
        @Override
        protected List<Task> doInBackground(Void... voids) {
            List<Task> tasks = new ArrayList<>();
            AppDatabase db = getDatabase();
            sqlTaskDao dao = db.sqltaskDao();
            for (sqlTask sTask : dao.getAll()) {
                tasks.add(new Task(sTask.title, sTask.description, sTask.isFavourite));
            }
            return tasks;
        }
    }

    public static class WriteDataTask extends AsyncTask<List<Task>, Integer, Void> {
        @SafeVarargs
        @Override
        protected final Void doInBackground(List<Task>... params) {
            AppDatabase db = getDatabase();
            sqlTaskDao dao = db.sqltaskDao();
            dao.delete();
            for (Task task : params[0]) {
                sqlTask sTask = new sqlTask();
                sTask.title = task.getTitle();
                sTask.description = task.getDescription();
                sTask.isFavourite = task.isFavourite();
                dao.insert(sTask);
            }
            return null;
        }
    }

    private static class ClearDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            AppDatabase db = getDatabase();
            sqlTaskDao dao = db.sqltaskDao();
            dao.delete();
            return null;
        }
    }
}
