package com.marukhinm.app_storage_async.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.marukhinm.app_storage_async.Task;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static android.content.SharedPreferences.Editor;

public class SharedPreferencesHelper implements Helper {
    private static final String DATA_PREF = "DATA_PREF";
    private static WeakReference<Context> sContext;

    SharedPreferencesHelper(Context context) {
        sContext = new WeakReference<>(context);
    }

    @Override
    public List<Task> load() {
        LoadDataTask load = new LoadDataTask();
        load.execute();
        try {
            return load.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public void write(List<Task> list) {
        WriteDataTask write = new WriteDataTask();
        write.execute(list);
    }

    @Override
    public void clear() {
        ClearDataTask clear = new ClearDataTask();
        clear.execute();
    }

    private static class LoadDataTask extends AsyncTask<Void, Integer, List<Task>> {
        @Override
        protected List<Task> doInBackground(Void... voids) {
            List<Task> tasks = new ArrayList<>();
            SharedPreferences pref = sContext.get().getSharedPreferences(DATA_PREF, Context.MODE_PRIVATE);
            int size = pref.getAll().size();
            for (int i = 0; i < size / 3; i++) {
                String title = pref.getString(i + "title", "");
                String description = pref.getString(i + "description", "");
                String favourite = pref.getString(i + "favourite", "");
                boolean isFavourite = Objects.equals(favourite, "true");
                tasks.add(new Task(title, description, isFavourite));
            }
            return tasks;
        }
    }

    private static class WriteDataTask extends AsyncTask<List<Task>, Integer, Void> {
        @SafeVarargs
        @Override
        protected final Void doInBackground(List<Task>... params) {
            SharedPreferences pref = sContext.get().getSharedPreferences(DATA_PREF, Context.MODE_PRIVATE);
            Editor editor = pref.edit();
            editor.clear();
            for (int i = 0; i < params[0].size(); i++) {
                Task task = params[0].get(i);
                editor.putString(i + "title", task.getTitle());
                editor.putString(i + "description", task.getDescription());
                if (task.isFavourite()) {
                    editor.putString(i + "favourite", "true");
                } else {
                    editor.putString(i + "favourite", "false");
                }
                editor.apply();
            }
            return null;
        }
    }

    private static class ClearDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            SharedPreferences pref = sContext.get().getSharedPreferences(DATA_PREF, Context.MODE_PRIVATE);
            Editor editor = pref.edit();
            editor.clear();
            editor.apply();
            return null;
        }
    }
}