package com.marukhinm.app_storage_async.storage;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;

import com.marukhinm.app_storage_async.Task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class ExternalInternalStorageHelper implements Helper {
    private static final String EXTERNAL_STORAGE = "EXTERNAL_STORAGE";
    private static final String INTERNAL_STORAGE = "INTERNAL_STORAGE";
    private static final String TYPE_STORAGE = "TYPE_STORAGE";
    private static final String TASKS = "TASKS";

    private static WeakReference<Context> sContext;
    private String mTypeStorage;

    ExternalInternalStorageHelper(Context context, String typeStorage) {
        sContext = new WeakReference<>(context);
        mTypeStorage = typeStorage;
    }

    @Override
    public List<Task> load() {
        LoadDataTask load = new LoadDataTask();
        load.execute(mTypeStorage);
        try {
            return load.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public void write(List<Task> list) {
        WriteDataTask write = new WriteDataTask();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE_STORAGE, mTypeStorage);
        bundle.putParcelableArrayList(TASKS, (ArrayList<? extends Parcelable>) list);
        write.execute(bundle);
    }

    @Override
    public void clear() {
        ClearDataTask clear = new ClearDataTask();
        clear.execute(mTypeStorage);
    }

    private static class LoadDataTask extends AsyncTask<String, Integer, List<Task>> {
        @Override
        protected List<Task> doInBackground(String... params) {
            File data = null;
            switch (params[0]) {
                case EXTERNAL_STORAGE:
                    data = new File(sContext.get().getExternalFilesDir(null), "data.txt");
                    break;
                case INTERNAL_STORAGE:
                    data = new File(sContext.get().getFilesDir(), "data.txt");
            }
            List<Task> tasks = new ArrayList<>();
            try {
                if (data != null && data.exists()) {
                    FileReader fileReader = new FileReader(data);
                    Scanner scanner = new Scanner(fileReader);
                    while (scanner.hasNextLine()) {
                        String[] line = scanner.nextLine().split("#/#");
                        tasks.add(new Task(line[0], line[1], line[2].equals("true")));
                    }
                    fileReader.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return tasks;
        }
    }

    private static class WriteDataTask extends AsyncTask<Bundle, Integer, Void> {
        @SafeVarargs
        @Override
        protected final Void doInBackground(Bundle... params) {
            File data = null;
            if (params == null || params.length == 0) {
                return null;
            }
            switch (Objects.requireNonNull(params[0].getString(TYPE_STORAGE))) {
                case EXTERNAL_STORAGE:
                    data = new File(sContext.get().getExternalFilesDir(null), "data.txt");
                    break;
                case INTERNAL_STORAGE:
                    data = new File(sContext.get().getFilesDir(), "data.txt");
            }
            try {
                if (data != null && !data.exists()) {
                    data.createNewFile();
                }
                FileWriter fileWriter = new FileWriter(data);
                for (Parcelable taskP : Objects.requireNonNull(params[0].getParcelableArrayList(TASKS))) {
                    Task task = (Task) taskP;
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder
                            .append(task.getTitle()).append("#/#").
                            append(task.getDescription()).append("#/#");
                    if (task.isFavourite()) {
                        stringBuilder.append("true").append("\n");
                    } else {
                        stringBuilder.append("false").append("\n");
                    }
                    fileWriter.write(stringBuilder.toString());
                }
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private static class ClearDataTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            File data = null;
            switch (params[0]) {
                case EXTERNAL_STORAGE:
                    data = new File(sContext.get().getExternalFilesDir(null), "data.txt");
                    break;
                case INTERNAL_STORAGE:
                    data = new File(sContext.get().getFilesDir(), "data.txt");
            }
            try {
                if (data != null && !data.exists()) {
                    data.createNewFile();
                }
                FileWriter fileWriter = new FileWriter(data);
                fileWriter.write("");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
