package com.marukhinm.app_storage_async.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.marukhinm.app_storage_async.Task;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static com.marukhinm.app_storage_async.SettingsActivity.EXTERNAL_STORAGE;
import static com.marukhinm.app_storage_async.SettingsActivity.FROM;
import static com.marukhinm.app_storage_async.SettingsActivity.INTERNAL_STORAGE;
import static com.marukhinm.app_storage_async.SettingsActivity.SETTINGS_PREF;
import static com.marukhinm.app_storage_async.SettingsActivity.SHARED_PREFERENCES;
import static com.marukhinm.app_storage_async.SettingsActivity.SQL;

public class StorageHelper {
    private Context mContext;
    private Helper mHelper;

    public StorageHelper(Context context) {
        mContext = context;
        mHelper = helperType();
    }

    private Helper helperType() {
        HelperTypeTask helperTypeTask = new HelperTypeTask();
        helperTypeTask.execute(mContext);
        try {
            return helperTypeTask.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new SharedPreferencesHelper(mContext);
    }

    private static class HelperTypeTask extends AsyncTask<Context, Void, Helper> {
        @Override
        protected Helper doInBackground(Context... params) {
            Helper helper = null;
            SharedPreferences mSettings = params[0].getSharedPreferences(SETTINGS_PREF,
                    Context.MODE_PRIVATE);
            if (mSettings.contains(FROM)) {
                switch (Objects.requireNonNull(mSettings.getString(FROM, ""))) {
                    case SHARED_PREFERENCES:
                        helper = new SharedPreferencesHelper(params[0]);
                        break;
                    case EXTERNAL_STORAGE:
                        helper = new ExternalInternalStorageHelper(params[0], EXTERNAL_STORAGE);
                        break;
                    case INTERNAL_STORAGE:
                        helper = new ExternalInternalStorageHelper(params[0], INTERNAL_STORAGE);
                        break;
                    case SQL:
                        helper = new SQLHelper(params[0]);
                }
            } else {
                helper = new SharedPreferencesHelper(params[0]);
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString(FROM, SHARED_PREFERENCES);
                editor.apply();
            }
            return helper;
        }
    }

    public List<Task> loadData() {
        return mHelper.load();
    }

    public void writeData(List<Task> data) {
        mHelper.write(data);
    }

    public void clearData() {
        mHelper.clear();
    }
}
