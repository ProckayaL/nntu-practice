package com.marukhinm.app_storage_async;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ProgressBar;

import com.marukhinm.app_storage_async.list.ListAdapter;
import com.marukhinm.app_storage_async.list.RecyclerViewClickListener;
import com.marukhinm.app_storage_async.storage.StorageHelper;
import com.marukhinm.app_storage_async.viewpager.CustomPagerAdapter;
import com.marukhinm.app_storage_async.viewpager.HandlerInterface;

import java.util.ArrayList;
import java.util.List;

import static com.marukhinm.app_storage_async.SettingsActivity.TASKS_EXTRA;
import static com.marukhinm.app_storage_async.TaskActivity.FAVOURITE_EXTRA;
import static com.marukhinm.app_storage_async.TaskActivity.POSITION_EXTRA;
import static com.marukhinm.app_storage_async.TaskActivity.TASK_EXTRA;

public class TaskListActivity extends AppCompatActivity implements View.OnClickListener,
        RecyclerViewClickListener, HandlerInterface {

    public static final String TYPE_TASK = "TYPE_TASK";
    public static final int NEW_TASK_TYPE = 1010;
    public static final int EDIT_TASK_TYPE = 1110;

    public static final String TASKS_LIST = "TASKS_LIST";

    private TabLayout mTabs;
    private ProgressBar mProgressBar;

    private List<Task> mAllTasks;
    private List<Task> mFavTasks;

    private RecyclerView mAllTaskList;
    private RecyclerView mFavTaskList;

    private ListAdapter mAllAdapter;
    private ListAdapter mFavAdapter;

    private StorageHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.tasks_title);

        mTabs = findViewById(R.id.tabs);
        mProgressBar = findViewById(R.id.progressBar);

        FloatingActionButton newTask = findViewById(R.id.newTask);
        newTask.setOnClickListener(this);

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this, mTabs.getTabCount()));
        mTabs.setupWithViewPager(viewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHelper = new StorageHelper(this);
        mAllTasks = mHelper.loadData();
        mFavTasks = new ArrayList<>();

        if (mAllAdapter != null && mFavAdapter != null) {
            configLists();
            updateList();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.newTask) {
            Intent intent = new Intent(this, TaskActivity.class)
                    .putExtra(FAVOURITE_EXTRA, mTabs.getSelectedTabPosition() != 0)
                    .putExtra(TYPE_TASK, NEW_TASK_TYPE)
                    .putParcelableArrayListExtra(TASKS_LIST, (ArrayList<Task>) mAllTasks);
            startActivity(intent);
        }
    }

    @Override
    public void onClickRV(final View view, final int position) {
        switch (view.getId()) {
            case R.id.itemLayout:
                startChangeTask(position);
                break;
            case R.id.menuItem:
                PopupMenu popupMenu = new PopupMenu(this, view);
                popupMenu.inflate(R.menu.menu_item_popup);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.change:
                                startChangeTask(position);
                                break;
                            case R.id.remove:
                                if (mTabs.getSelectedTabPosition() == 1) {
                                    mAllTasks.remove(positionInAllTasks(mFavTasks
                                            .get(position)));
                                } else {
                                    mAllTasks.remove(position);
                                }
                                break;
                            case R.id.invFavorites:
                                invertFavourite(position);
                        }
                        updateList();
                        mHelper.writeData(mAllTasks);
                        return false;
                    }
                });
                popupMenu.show();
                break;
            case R.id.favouriteImage:
                invertFavourite(position);
                updateList();
                mHelper.writeData(mAllTasks);
        }
    }

    private void startChangeTask(int position) {
        Intent intent = new Intent(TaskListActivity.this, TaskActivity.class)
                .putExtra(TYPE_TASK, EDIT_TASK_TYPE)
                .putParcelableArrayListExtra(TASKS_LIST, (ArrayList<Task>) mAllTasks);
        if (mTabs.getSelectedTabPosition() == 1) {
            position = positionInAllTasks(mFavTasks.get(position));
        }
        intent.putExtra(POSITION_EXTRA, position).putExtra(TASK_EXTRA, mAllTasks.get(position));
        startActivity(intent);
    }

    private void invertFavourite(int position) {
        Task task;
        if (mTabs.getSelectedTabPosition() == 1) {
            task = mAllTasks.get(positionInAllTasks(mFavTasks.get(position)));
        } else {
            task = mAllTasks.get(position);
        }
        task.setFavourite(!task.isFavourite());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putParcelableArrayListExtra(TASKS_EXTRA,
                    (ArrayList<? extends Parcelable>) mAllTasks);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateList() {
        mFavTasks.clear();
        for (Task task : mAllTasks) {
            if (task.isFavourite()) {
                mFavTasks.add(task);
            }
        }
        mAllAdapter.notifyDataSetChanged();
        mFavAdapter.notifyDataSetChanged();
    }

    private int positionInAllTasks(Task task) {
        for (int i = 0; i < mAllTasks.size(); i++) {
            if (mAllTasks.get(i) == task) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void sendData(final ViewGroup group) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (group.getTag().equals("tab0")) {
                    mAllTaskList = group.findViewById(R.id.taskList);
                } else {
                    mFavTaskList = group.findViewById(R.id.taskList);
                }
                if (mAllTaskList != null && mFavTaskList != null) {
                    mProgressBar.setVisibility(View.GONE);
                    configLists();
                    updateList();
                }
            }
        });
    }

    private void configLists() {
        mAllTaskList.setLayoutManager(new LinearLayoutManager(TaskListActivity.this));
        mAllAdapter = new ListAdapter(TaskListActivity.this, mAllTasks);
        mAllTaskList.setAdapter(mAllAdapter);

        mFavTaskList.setLayoutManager(new LinearLayoutManager(TaskListActivity.this));
        mFavAdapter = new ListAdapter(TaskListActivity.this, mFavTasks);
        mFavTaskList.setAdapter(mFavAdapter);
    }
}