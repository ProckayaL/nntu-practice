package com.marukhinm.app_storage_async.viewpager;

import android.view.ViewGroup;

public interface HandlerInterface {
    void sendData(ViewGroup group);
}
