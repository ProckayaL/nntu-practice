package com.marukhinm.app_storage_async.viewpager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marukhinm.app_storage_async.R;

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    private int mCount;

    public CustomPagerAdapter(Context context, int count) {
        mContext = context;
        mCount = count;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.tab, container, false);
        layout.setTag("tab" + position);
        container.addView(layout);
        ((HandlerInterface) mContext).sendData(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.tabAll_text);
        } else {
            return mContext.getString(R.string.tabFavourite_text);
        }
    }
}
