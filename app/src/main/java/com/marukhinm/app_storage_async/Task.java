package com.marukhinm.app_storage_async;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable {
    private String mTitle;
    private String mDescription;
    private boolean mIsFavourite;

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel source) {
            String title = source.readString();
            String description = source.readString();
            boolean isFavourite = source.readByte() != 0;
            return new Task(title, description, isFavourite);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public Task(String title, String description, boolean isFavourite) {
        mTitle = title;
        mDescription = description;
        mIsFavourite = isFavourite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDescription);
        dest.writeByte((byte) (mIsFavourite ? 1 : 0));
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public boolean isFavourite() {
        return mIsFavourite;
    }

    void setFavourite(boolean favourite) {
        mIsFavourite = favourite;
    }

    public String toString() {
        return mTitle + "  " + mDescription + "  " + mIsFavourite;
    }
}
