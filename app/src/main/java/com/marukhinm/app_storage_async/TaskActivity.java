package com.marukhinm.app_storage_async;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.marukhinm.app_storage_async.storage.StorageHelper;

import java.util.List;

import static com.marukhinm.app_storage_async.TaskListActivity.EDIT_TASK_TYPE;
import static com.marukhinm.app_storage_async.TaskListActivity.NEW_TASK_TYPE;
import static com.marukhinm.app_storage_async.TaskListActivity.TASKS_LIST;
import static com.marukhinm.app_storage_async.TaskListActivity.TYPE_TASK;

public class TaskActivity extends AppCompatActivity {
    public static final String POSITION_EXTRA = "POSITION_EXTRA";
    public static final String TASK_EXTRA = "TASK_EXTRA";
    public final static String FAVOURITE_EXTRA = "FAVOURITE_EXTRA";

    private Intent mIntent;
    private EditText mTitle;
    private EditText mDescription;
    private CheckBox mIsFavourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.new_task_title);

        mTitle = findViewById(R.id.titleField);
        mDescription = findViewById(R.id.descriptionField);
        mIsFavourite = findViewById(R.id.favourite);

        mIntent = getIntent();

        if (mIntent != null && mIntent.hasExtra(FAVOURITE_EXTRA)) {
            mIsFavourite.setChecked(mIntent.getBooleanExtra(FAVOURITE_EXTRA, false));
        } else if (mIntent != null && mIntent.hasExtra(TASK_EXTRA)) {
            getSupportActionBar().setTitle(R.string.edit_task_title);
            Task task = mIntent.getParcelableExtra(TASK_EXTRA);
            mTitle.setText(task.getTitle());
            mDescription.setText(task.getDescription());
            mIsFavourite.setChecked(task.isFavourite());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            if (!mTitle.getText().toString().isEmpty()) {
                Task task = new Task(mTitle.getText().toString(),
                        mDescription.getText().toString(), mIsFavourite.isChecked());

                List<Task> tasks;
                StorageHelper helper = new StorageHelper(this);
                if (mIntent.hasExtra(TASKS_LIST)) {
                    tasks = mIntent.getParcelableArrayListExtra(TASKS_LIST);
                } else {
                    tasks = helper.loadData();
                }

                if (mIntent.hasExtra(TYPE_TASK)) {
                    switch (mIntent.getIntExtra(TYPE_TASK, 0)) {
                        case NEW_TASK_TYPE:
                            tasks.add(task);
                            Toast.makeText(this, R.string.task_created, Toast.LENGTH_SHORT).show();
                            break;
                        case EDIT_TASK_TYPE:
                            int position = mIntent.getIntExtra(POSITION_EXTRA, -1);
                            if (position != -1) {
                                tasks.set(position, task);
                                Toast.makeText(this, R.string.task_edited, Toast.LENGTH_SHORT).show();
                            }
                    }
                    helper.writeData(tasks);
                }
                finish();
            } else {
                Toast.makeText(this, R.string.fill_field, Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
