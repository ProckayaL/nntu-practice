package com.marukhinm.app_storage_async;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.marukhinm.app_storage_async.storage.StorageHelper;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static android.content.SharedPreferences.Editor;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String SETTINGS_PREF = "SETTINGS_PREF";
    public static final String FROM = "FROM";
    public static final String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static final String EXTERNAL_STORAGE = "EXTERNAL_STORAGE";
    public static final String INTERNAL_STORAGE = "INTERNAL_STORAGE";
    public static final String SQL = "SQL";

    public static final String TASKS_EXTRA = "TASKS_EXTRA";

    private RadioGroup mRadioGroup;
    private int mStartChecked;
    private static int sCheckedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.settings_title);

        mRadioGroup = findViewById(R.id.radioGroup);
        initRadioGroup();
        mStartChecked = mRadioGroup.getCheckedRadioButtonId();

        Button move = findViewById(R.id.move);
        move.setOnClickListener(this);
    }

    private void initRadioGroup() {
        InitRadioGroupTask initRadioGroupTask = new InitRadioGroupTask();
        initRadioGroupTask.execute(this);
        try {
            mRadioGroup.check(initRadioGroupTask.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void move() {
        int checked = mRadioGroup.getCheckedRadioButtonId();
        if (mStartChecked != checked) {
            StorageHelper oldHelper = new StorageHelper(this);
            sCheckedButton = mRadioGroup.getCheckedRadioButtonId();
            MoveTask moveTask = new MoveTask();
            moveTask.execute(this);
            StorageHelper newHelper = new StorageHelper(this);
            List<Task> tasks = getIntent().getParcelableArrayListExtra(TASKS_EXTRA);
            newHelper.writeData(tasks);
            oldHelper.clearData();
            mStartChecked = checked;
            Toast.makeText(this, R.string.data_moved, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.another_storage, Toast.LENGTH_LONG).show();
        }
    }

    private static class InitRadioGroupTask extends AsyncTask<Context, Void, Integer> {
        @Override
        protected Integer doInBackground(Context... params) {
            SharedPreferences mSettingsPref = params[0].getSharedPreferences(SETTINGS_PREF, MODE_PRIVATE);
            int buttonId = -1;
            if (mSettingsPref.contains(FROM)) {
                switch (Objects.requireNonNull(mSettingsPref.getString(FROM, ""))) {
                    case SHARED_PREFERENCES:
                        buttonId = R.id.sharedPreferences;
                        break;
                    case EXTERNAL_STORAGE:
                        buttonId = R.id.externalStorage;
                        break;
                    case INTERNAL_STORAGE:
                        buttonId = R.id.internalStorage;
                        break;
                    case SQL:
                        buttonId = R.id.sql;
                }
            }
            return buttonId;
        }
    }

    private static class MoveTask extends AsyncTask<Context, Void, Void> {
        @Override
        protected Void doInBackground(Context... params) {
            SharedPreferences mSettingsPref = params[0].getSharedPreferences(SETTINGS_PREF, MODE_PRIVATE);
            Editor editor = mSettingsPref.edit();
            switch (sCheckedButton) {
                case R.id.sharedPreferences:
                    editor.putString(FROM, SHARED_PREFERENCES);
                    break;
                case R.id.externalStorage:
                    editor.putString(FROM, EXTERNAL_STORAGE);
                    break;
                case R.id.internalStorage:
                    editor.putString(FROM, INTERNAL_STORAGE);
                    break;
                case R.id.sql:
                    editor.putString(FROM, SQL);
            }
            editor.apply();
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.move) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.dialog_title)
                    .setMessage(R.string.dialog_message)
                    .setCancelable(false)
                    .setPositiveButton(R.string.dialog_pos_button,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    move();
                                    dialog.cancel();
                                }
                            })
                    .setNegativeButton(R.string.dialog_neg_button,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }
}
