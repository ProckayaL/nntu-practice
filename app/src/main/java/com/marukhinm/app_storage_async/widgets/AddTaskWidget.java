package com.marukhinm.app_storage_async.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.marukhinm.app_storage_async.R;
import com.marukhinm.app_storage_async.TaskActivity;

import static com.marukhinm.app_storage_async.TaskListActivity.NEW_TASK_TYPE;
import static com.marukhinm.app_storage_async.TaskListActivity.TYPE_TASK;

public class AddTaskWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        for (int appWidgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_add_task);

        Intent intent = new Intent(context, TaskActivity.class).putExtra(TYPE_TASK, NEW_TASK_TYPE);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 104, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        remoteViews.setOnClickPendingIntent(R.id.addTask, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }
}
